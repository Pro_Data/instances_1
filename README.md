# Robust operating room planning considering upstream and downstream units: A new two-stage heuristic algorithm #

This repository includes instances and solutions for both of the certainty and uncertainty conditions in the paper entitled "Robust operating room planning considering upstream and downstream units: A new two-stage heuristic algorithm".

Note that all indices, scalars, and parameters are defined in a way that intersted readers can relate them to the ones provided in the paper.


# Certainty Condition #


1-20: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_1.txt

2-20: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_2.txt

3-25: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_3.txt

4-25: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_4.txt

5-30: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_5.txt

6-30: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_6.txt

7-35: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_7.txt

8-35: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_8.txt

9-40: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_9.txt

10-40: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_10.txt

11-45: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_11.txt

12-45: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_12.txt

13-50: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_13.txt

14-50: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_14.txt

15-55: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_15.txt

16-55: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_16.txt

17-60: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_17.txt

18-60: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_18.txt

19-70: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_19.txt

20-70: https://bitbucket.org/Pro_Data/instances_1/downloads/Certainty_20.txt




# Unertainty Condition #


21-20: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_21.txt

22-20: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_22.txt

23-25: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_23.txt

24-25: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_24.txt

25-30: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_25.txt

26-30: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_26.txt

27-35: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_27.txt

28-35: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_28.txt

29-40: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_29.txt

30-40: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_30.txt

31-45: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_31.txt

32-45: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_32.txt

33-50: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_33.txt

34-50: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_34.txt

35-55: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_35.txt

36-55: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_36.txt

37-60: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_37.txt

38-60: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_38.txt

39-70: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_39.txt

40-70: https://bitbucket.org/Pro_Data/instances_1/downloads/Uncertainty_40.txt